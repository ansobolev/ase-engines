import sys
import os

# dir_path = os.path.dirname(os.path.realpath(__file__))
# sys.path.insert(0, dir_path)

import pytest

from .aims import groundstate

import os
import shutil

import numpy as np
from pathlib import Path
from ase.build import bulk

from ase.io.aims import read_aims_output
from tempfile import TemporaryDirectory


def test_groundstate(tmpdir):
    atoms = bulk("Si")
    parameters = dict(
        xc="pw-lda",
        k_grid=[4, 4, 4],
        species_dir=os.environ.get("AIMS_SPECIES_DIR"),
    )

    gs = groundstate(
        atoms,
        parameters,
        tmpdir,
        ["energy", "forces", "stress"],
    )

    gs.copy_elsi_restart(Path(f"{tmpdir}/aims_copy/"))
    gs.copy_elsi_restart(Path(f"{tmpdir}/aims_copy/"), True)

    read_atoms = read_aims_output(f"{tmpdir}/aims.out")

    assert np.sum(np.abs(read_atoms.get_forces().flatten())) < 1e-7
    assert int(np.round(read_atoms.get_potential_energy())) == -15696
    assert np.allclose(
        read_atoms.get_stress(), [0.00337464, 0.00337465, 0.00337465, 0.0, 0.0, 0.0]
    )


if __name__ == "__main__":
    test_groundstate()

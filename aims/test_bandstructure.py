import sys
import os

# dir_path = os.path.dirname(os.path.realpath(__file__))
# sys.path.insert(0, dir_path)

import pytest

from .aims import groundstate, bandstructure, bandstructure_single_shot

import os
import shutil

import numpy as np
from pathlib import Path
from ase.build import bulk

from ase.io.aims import read_aims_output
from tempfile import TemporaryDirectory


@pytest.fixture
def atoms():
    return bulk("Si")


@pytest.fixture
def parameters():
    return dict(
        xc="pw-lda",
        k_grid=[4, 4, 4],
        species_dir=os.environ.get("AIMS_SPECIES_DIR"),
    )


def test_bandstructure_single_shot(atoms, parameters, tmpdir):
    bandpath = atoms.cell.bandpath(density=10)

    bs = bandstructure_single_shot(
        atoms,
        bandpath,
        parameters,
        tmpdir,
    )


def test_bandstructure(atoms, parameters, tmpdir):
    bandpath = atoms.cell.bandpath(density=10)

    gs = groundstate(
        atoms,
        parameters,
        Path(tmpdir) / "groundstate/",
    )

    bs = bandstructure(
        gs,
        bandpath,
        parameters,
        Path(tmpdir) / "bandstructure",
    )


if __name__ == "__main__":
    test_groundstate()

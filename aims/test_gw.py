""" A collection of tests for GW AIMS calculation
"""
import json
import hashlib
import os
import shutil
from pathlib import Path
from tempfile import TemporaryDirectory
import numpy as np
import pytest

from ase.build import bulk, molecule
from ase.calculators.aims import Aims

from .aims import gw

OUTPUT_FILES_DIR = Path(__file__).parent / "output_files"


@pytest.fixture
def Li():
    return bulk("Li")


@pytest.fixture
def cluster():
    return molecule("C2H4")


@pytest.fixture
def parameters():
    return {
        "run_command": "mpirun -np 4 aims.x",
        "xc": "pbe",
        "k_grid": [4, 4, 4],
    }


def mocked_calculate(atoms, parameters, directory):
    """A mocked Aims calculate method. It calculates the hash of the inputs and copies over"""

    def md5_hash(some_dict):
        """Hashing the inputs"""
        dhash = hashlib.md5()
        # We need to sort arguments so {'a': 1, 'b': 2} is
        # the same as {'b': 2, 'a': 1}
        encoded = json.dumps(some_dict, sort_keys=True).encode()
        dhash.update(encoded)
        return dhash.hexdigest()

    # serialize atoms and parameters, get a hash
    for key in ("species_dir", "run_command"):
        if key in parameters:
            parameters.pop(key)
    atoms_dict = {
        k: v.tolist() if isinstance(v, np.ndarray) else v
        for k, v in atoms.todict().items()
    }
    inputs_hash = md5_hash({"atoms": atoms_dict, "parameters": parameters})
    if inputs_hash in os.listdir(OUTPUT_FILES_DIR):
        for output_file in (OUTPUT_FILES_DIR / inputs_hash).glob("*"):
            if output_file.is_file():
                shutil.copy(output_file, directory)


def test_1d_2d(parameters, tmp_path):
    """Test against 1d and 2d structures"""
    from ase.build import nanotube, bcc100

    with pytest.raises(ValueError):
        atoms = nanotube(3, 3)
        gw(atoms, parameters, tmp_path)
    with pytest.raises(ValueError):
        atoms = bcc100("Li", [2, 2, 2])
        gw(atoms, parameters, tmp_path)


def test_3d_error(Li, parameters, tmp_path):
    """Test bulk structure with wrong parameters"""
    parameters["qpe_calc"] = "gw"
    with pytest.raises(ValueError):
        gw(Li, parameters, tmp_path)
    parameters["qpe_calc"] = "gw_expt"
    with pytest.raises(KeyError):
        gw(Li, parameters, tmp_path)


@pytest.mark.skipif(not OUTPUT_FILES_DIR.exists(), reason="No output data")
def test_3d(Li, parameters, tmp_path, mocker):
    """Test bulk structure"""
    parameters["qpe_calc"] = "gw_expt"
    parameters["anacon_type"] = "two-pole"
    mocked_calculator = mocker.patch.object(Aims, "calculate")
    mocked_calculator.side_effect = lambda i, j, k: mocked_calculate(
        Li, parameters, tmp_path
    )
    calc = gw(Li, parameters, tmp_path)
    eigenvalues = calc.read_results()["eigenvalues"]
    assert eigenvalues.shape == (64, 11, 1)
    assert eigenvalues[0, 0, 0] == -51.48795


def test_0d_error(cluster, parameters, tmp_path):
    """Test 0-D structure with wrong parameters"""
    parameters.pop("k_grid")
    parameters["qpe_calc"] = "gw_expt"
    with pytest.raises(ValueError):
        gw(cluster, parameters, tmp_path)
    parameters["qpe_calc"] = "gw"
    with pytest.raises(KeyError):
        gw(cluster, parameters, tmp_path)
    parameters["anacon_type"] = "something"
    with pytest.raises(KeyError):
        gw(cluster, parameters, tmp_path)


@pytest.mark.skipif(not OUTPUT_FILES_DIR.exists(), reason="No output data")
def test_0d(cluster, parameters, tmp_path, mocker):
    """Test 0-D structure"""
    parameters.pop("k_grid")
    parameters["qpe_calc"] = "gw"
    parameters["anacon_type"] = "two-pole"
    mocked_calculator = mocker.patch.object(Aims, "calculate")
    mocked_calculator.side_effect = lambda i, j, k: mocked_calculate(
        cluster, parameters, tmp_path
    )
    calc = gw(cluster, parameters, tmp_path)
    eigenvalues = calc.read_results()["eigenvalues"]
    assert eigenvalues.shape == (1, 48, 1)
    assert eigenvalues[0, 0, 0] == -268.95434

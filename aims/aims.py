import os
from pathlib import Path

from ase.build import bulk
from ase.calculators.aims import AimsProfile, AimsTemplate
from ase.data import atomic_numbers
from ase.dft.kpoints import resolve_kpt_path_string, kpoint_convert
from ase.io.aims import read_aims_output

class AimsCalculation:
    def __init__(self, atoms, parameters, directory):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = Path(directory)

    def copy_elsi_restart(self, directory, overwrite=False):
        directory.mkdir(parents=True, exist_ok=overwrite)
        restart_filenames = directory.glob(f"*.csc")
        for file in restart_filenames:
            file.copy(directory)

    def read_results(self):
        template = AimsTemplate()
        return template.read_results(self.directory)


def check_directory(directory, overwrite):
    directory = Path(directory)
    directory.mkdir(exist_ok=True, parents=True)
    if not overwrite and (directory / "aims.out").exists():
        raise IOError("Calculation already exists in the directory.")

    return directory


def ase2aims_bandpath(bandpath):
    """following the implementation in aims-clims tool"""

    r_kpts = resolve_kpt_path_string(bandpath.path, bandpath.special_points)

    linesAndLabels = []
    k_index = -1

    for points, labels in zip(r_kpts[1], bandpath.path.split(",")):
        k_index += 1
        n_points = []
        for pt in points[1:]:
            k_start = k_index
            while np.any(bandpath.kpts[k_index] != pt):
                k_index += 1
            n_points.append(k_index - k_start + 1)

        linesAndLabels.append(
            [n_points, labels[:-1], labels[1:], points[:-1], points[1:]]
        )

    bands = []
    for segs in linesAndLabels:
        for points, lstart, lend, start, end in zip(*segs):
            bands.append(
                "band {:9.5f}{:9.5f}{:9.5f} {:9.5f}{:9.5f}{:9.5f} {:4} {:3}{:3}".format(
                    *start, *end, points, lstart, lend
                )
            )

    return bands

def make_profile(argv=None):
    """Create an AimsProfile object from the run command

    Parameters
    ----------
    argv: str or list
        The list of command line arguments to run FHI-aims

    Returns
    -------
    AimsProfile
        The profile object to run FHI-aims
    """

    # Should be a part of the engines configuration file
    if argv is None:
        argv = os.getenv("ASE_AIMS_COMMAND", "aims.x").split()
    elif isinstance(argv, str):
        argv = argv.split()

    return AimsProfile(argv)


def set_species_to(
    atoms, working_direc=Path.cwd(), base_species_direc=None, species_parameters=None
):
    """Set the basis set information for FHI-aims

    Parameters
    ----------
    atoms: ase.Atoms
        The atoms object for the structure
    working_direc: str or Path
        The working directory
    base_species_direc: str or Path
        directory to store the basis set files for FHI-aims
    species_parameters: dict
        atom_species (str): basis_set type (str)

    Returns
    -------
    Path
        The directory to use as the species_dir in the calculator
    """

    # Should be in engines configuration file
    if base_species_direc is None:
        base_species_direc = os.environ.get("AIMS_SPECIES_DIR")

    if base_species_direc is None:
        raise ValueError(f"base_species_direc not definable")

    base_species_direc = Path(base_species_direc)
    working_direc = Path(working_direc)

    # Should be in engines configuration file
    if species_parameters is None:
        species_parameters = {}

    # Should be configurable really bad to do this way
    if "default" not in species_parameters:
        if len(species_parameters) > 0:
            species_parameters["default"] = list(species_parameters.values())[0]
        else:
            species_parameters["default"] = "light"

    uniques_species = set(atoms.symbols)
    numbers = [atomic_numbers[symb] for symb in uniques_species]
    if len(species_parameters) == 1 or np.all(
        np.array(species_parameters.values()) == list(species_parameters.values())[0]
    ):
        if all(
            [
                (base_species_direc / f"{number:02}_{symb}_default").exists()
                for symb, number in zip(uniques_species, numbers)
            ]
        ):
            return base_species_direc
        elif all(
            [
                (
                    base_species_direc
                    / species_parameters["default"]
                    / f"{number:02}_{symb}_default"
                ).exists()
                for symb, number in zip(uniques_species, numbers)
            ]
        ):
            return base_species_direc / species_parameters["default"]
        else:
            raise IOError("Basis set information for all species is not provided")

    # Possible to make this configurable this is a temp direc
    basis_dir = working_direc / "basissets/"
    basis_dir.mkdir(exist_ok=True)

    for symb, number in zip(uniques_species, numbers):
        check_file_base = base_species_direc / f"{number:02}_{symb}_default"
        if check_file_base.exists():
            check_file_base.copy(basis_dir)
            continue

        default_set = species_parameters.get(symb, species_parameters["default"])
        check_file_default_sets = (
            base_species_direc / default_set / f"{number:02}_{symb}_default"
        )
        if check_file_default_sets.exists():
            check_file_default_sets.copy(basis_dir)
            continue

        raise IOError(
            f"Basis set information for the species {symb} is not provided in the available directory: {base_species_direc}."
        )
    return basis_dir


def groundstate(atoms, parameters, directory, properties=["energy"], overwrite=False):
    parameters = parameters.copy()
    directory = check_directory(directory, overwrite)

    # TARP: Unless specified differently write the internal eigenstates (saved for ELSI restarts) to be read in for future calculations
    if "elsi_restart" not in parameters:
        parameters["elsi_restart"] = "read_and_write 1000"

    # Nothing in the parameters corresponds to species_parameters, right now set it to None this should change
    parameters["species_dir"] = set_species_to(
        atoms, directory, parameters.get("species_dir", None), None
    )

    profile = make_profile(parameters.pop("run_command", None))
    template = AimsTemplate()

    template.write_input(directory, atoms, parameters, properties)
    template.execute(directory, profile)

    return AimsCalculation(atoms, parameters, directory)


def bandstructure_single_shot(atoms, bandpath, parameters, directory, overwrite=False):
    directory = check_directory(directory, overwrite)
    aims_bandpath = ase2aims_bandpath(bandpath)
    outputs = parameters.pop("output", list()) + aims_bandpath
    parameters["output"] = outputs

    return groundstate(atoms, parameters, directory)


def bandstructure(gs, bandpath, parameters, directory, overwrite=False):
    try:
        gs.copy_elsi_restart(directory)
    except FileExistsError:
        pass

    return bandstructure_single_shot(gs.atoms, bandpath, parameters, directory)


def gw(atoms, parameters, directory, bandpath=None, overwrite=False):
    """A GW calculation for aims

    Args:
        atoms (ase.Atoms): a relaxed Atoms object to calculate GW for
        parameters (dict): general parameters for the calculation
        directory (:obj:`list` or :obj:`pathlib.Path`): a directory to put the calculation into
        bandpath (list): a band path to calculate the GS band structure for
        overwrite (boolean): a directory overwrite flag

    Returns:
        an AimsCalculation object
    """

    parameters = parameters.copy()
    directory = check_directory(directory, overwrite)

    # checks
    if sum(atoms.pbc) not in (0, 3):
        raise ValueError(
            "GW calculation can be performed only on a bulk or cluster geometry"
        )

    if all(atoms.pbc):
        if "qpe_calc" not in parameters:
            parameters["qpe_calc"] = "gw_expt"
        elif parameters["qpe_calc"] != "gw_expt":
            raise ValueError("`qpe_calc` has to be `gw_expt` for periodic calculations")
    else:
        if bandpath is not None:
            raise ValueError('The band structure for a cluster geometry is not defined')
        if "qpe_calc" not in parameters:
            parameters["qpe_calc"] = "gw"
        elif ("sc_self_energy" not in parameters) and (
            parameters["qpe_calc"] not in ("gw", "ev_scgw", "ev_scgw0", "mp2")
        ):
            raise ValueError("`qpe_calc` is not correct for cluster calculation")

    if all(atoms.pbc):
        if bandpath is None:
            #  let the default k-grid density be 30
            bandpath = atoms.cell.bandpath(density=parameters.get('k_grid_density', 30))

        aims_bandpath = ase2aims_bandpath(bandpath)
        outputs = parameters.pop("output", list()) + aims_bandpath
        parameters["output"] = outputs

    if ("anacon_type" not in parameters) or (
        str(parameters["anacon_type"]) not in ("two-pole", "pade", "0", "1")
    ):
        raise KeyError(
            "GW calc should have `anacon_type` parameter with the value from [`two-pole`, `pade`]"
        )

    # Nothing in the parameters corresponds to species_parameters, right now set it to None this should change
    parameters["species_dir"] = set_species_to(
        atoms, directory, parameters.get("species_dir", None), None
    )

    profile = make_profile(parameters.pop("run_command", None))
    template = AimsTemplate()

    template.write_input(directory, atoms, parameters, ["energy"])
    template.execute(directory, profile)

    return AimsCalculation(atoms, parameters, directory)

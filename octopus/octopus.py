import copy
from pathlib import Path
import shutil

from ase.calculators.octopus import OctopusProfile, OctopusTemplate
from ase.build import bulk


# Needs to be serializable.
# Serialized atoms/parameters need to go into directory in a JSON file.
#
# Perhaps this should be split into OctopusLoader which is only
# aware of Octopus stuff (no ASE awareness) versus a normalizing
# layer which has atoms/parameters etc., and is a GroundState.
# That way, GroundState, BandStructure etc. can delegate to
# OctopusCalculation, EspressoCalculation etc. and essentially
# be different views of similar objects.
class OctopusCalculation:
    def __init__(self, atoms, parameters, directory):
        self._atoms = atoms.copy()
        self._parameters = copy.deepcopy(parameters)
        self.directory = directory

    def parameters(self):
        return copy.deepcopy(self._parameters)

    def atoms(self):
        return self._atoms.copy()

    def copy_restart_directory(self, directory):
        # Some things fail if there is no static/info, but it is not
        # actually needed.  We should be able to only clone the
        # "restart" folder.
        directory.mkdir(parents=True, exist_ok=True)
        shutil.copytree(self.directory / 'restart', directory / 'restart')

    def read_static_eigenvalues(self):
        from ase.io.octopus.output import read_eigenvalues_file
        eigpath = self.directory / 'static/eigenvalues'
        with eigpath.open() as fd:
            kpts, eigs, occs = read_eigenvalues_file(fd)
        return {'kpts': kpts, 'eigenvalues': eigs, 'occupations': occs}


def groundstate(atoms, parameters, directory):
    profile = OctopusProfile(['octopus'])
    template = OctopusTemplate()

    # XXX the 'properties' argument is actually unused.
    directory.mkdir(exist_ok=True)
    template.write_input(directory, atoms, parameters, properties=['energy'])
    template.execute(directory, profile)
    return OctopusCalculation(atoms, parameters, directory)


def bandstructure(gs, bandpath, parameters, directory):
    atoms = gs.atoms()

    # (We should verify that bandpath has same cell as atoms.)

    parameters = {
        **gs.parameters(),
        'calculationmode': 'unocc',
        'kpointsusesymmetries': False,
        'kpts': bandpath.kpts,
        **parameters}

    # Avoid leftover kpoint specifications from GS.
    # How do we do this kind of thing in general?
    # Caller should have high level of freedom to override
    # the GS parameters.
    parameters.pop('kpointsgrid', None)

    try:
        gs.copy_restart_directory(directory)
    except FileExistsError:
        pass

    profile = OctopusProfile(['octopus'])
    template = OctopusTemplate()

    template.write_input(directory, atoms, parameters, properties=['energy'])
    template.execute(directory, profile)
    return OctopusCalculation(atoms, parameters, directory)


def main():
    directory = Path('work/octopus').resolve()
    directory.mkdir(exist_ok=True, parents=True)

    atoms = bulk('Si')
    parameters = dict(
        calculationmode='gs',
        kpointsgrid=[[4, 4, 4]],
        kpointsusesymmetries=True,
        extrastates=1,
        Spacing='0.5')

    bandpath = atoms.cell.bandpath(density=12)

    print('groundstate')
    gs = groundstate(atoms, parameters, directory / 'groundstate')
    print('bandstructure')

    parameters = {
        'extrastates': 10,
        'ExtraStatesToConverge': 5}

    bscalculation = bandstructure(
        gs=gs, bandpath=bandpath,
        parameters=parameters,
        directory=directory / 'bandstructure')

    from ase.spectrum.band_structure import BandStructure

    results = bscalculation.read_static_eigenvalues()

    assert len(bandpath.kpts) == len(results['kpts'])
    maxerr = abs(bandpath.kpts - results['kpts']).max()
    assert maxerr < 1e-5

    eigenvalues = results['eigenvalues'].transpose(1, 0, 2)
    bs = BandStructure(bandpath, eigenvalues)
    bs.write(directory / 'bs.json')


if __name__ == '__main__':
    main()

from pathlib import Path
import shutil

from ase.build import bulk
from ase.calculators.espresso import (
    EspressoProfile, EspressoTemplate)


class EspressoCalculation:
    def __init__(self, atoms, parameters, directory):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = directory

    def copy_pwscf_savedir(self, directory):
        name = 'pwscf.save'
        shutil.copytree(self.directory / name,
                        directory / name)

    def read_results(self):
        template = EspressoTemplate()
        return template.read_results(self.directory)


def groundstate(atoms, parameters, directory):
    profile = EspressoProfile(['pw.x'])
    template = EspressoTemplate()

    directory.mkdir(exist_ok=True, parents=True)
    template.write_input(directory, atoms, parameters, ['energy'])
    template.execute(directory, profile)
    return EspressoCalculation(atoms, parameters, directory)


def bandstructure(gs, bandpath, parameters, directory):
    # path.write('path.json')

    atoms = gs.atoms.copy()

    directory.mkdir(exist_ok=True, parents=True)

    try:
        gs.copy_pwscf_savedir(directory)
    except FileExistsError:
        pass  # XXX unsafe

    # print('efermi', efermi)

    bsparameters = {
        **gs.parameters,
        'kpts': bandpath,
        'fixdensity': True,
        **parameters}

    bsparameters['input_data']['control'].update(
        calculation='bands',
        restart_mode='restart',
        verbosity='high')

    profile = EspressoProfile(['pw.x'])
    template = EspressoTemplate()

    template.write_input(directory, atoms, bsparameters, ['energy'])
    template.execute(directory, profile)
    return EspressoCalculation(atoms, bsparameters, directory)


def main():
    directory = Path('work/espresso')

    atoms = bulk('Si')

    pseudo_dir = Path(
        '/home/askhl/src/ase-datafiles/asetest/datafiles/espresso'
        '/gbrv-lda-espresso')

    pseudopotentials = {}
    # Yarrrrgh copy-pasted from ase/test/factories
    for ppath in pseudo_dir.glob('*.UPF'):
        fname = ppath.name
        symbol = fname.split('_', 1)[0].capitalize()
        pseudopotentials[symbol] = fname

    kwargs = dict(
        kpts=[4, 4, 4], input_data={'control': {}},
        ecutwfc=600 / 27.211,
        pseudo_dir=str(pseudo_dir),
        pseudopotentials=pseudopotentials)

    gs = groundstate(atoms, kwargs, directory / 'groundstate')

    bandpath = atoms.cell.bandpath('GXWK', density=15)
    bscalc = bandstructure(gs, bandpath, {}, directory / 'bandstructure')

    # print(bscalc)
    results = bscalc.read_results()
    eigenvalues = results['eigenvalues']
    espresso_kpts = results['ibz_kpoints']

    kpts_err = abs(bandpath.kpts - espresso_kpts).max()

    from ase.spectrum.band_structure import BandStructure
    # XXX we also should get the Fermi level
    bs = BandStructure(bandpath, eigenvalues)
    bs.write(directory / 'bs.json')
    print(kpts_err)
    assert kpts_err < 1e-7, kpts_err
    print(eigenvalues.shape)
    print(list(results))


if __name__ == '__main__':
    main()

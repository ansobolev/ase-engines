from setuptools import setup, find_packages


setup(name='ase_engines',
      description='prototypes for run modes with ASE codes',
      version='0.1',
      packages=find_packages())
